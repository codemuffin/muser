# MUSER 1.0.0

Reset passwords, create new users, list all users, all from outside the WP admin.

Only needs FTP access.

### Usage

Update 2 files first:

- `key.php` - Set a [random string](https://passwordsgenerator.net/)
- `user.php` - Set your login details. Only needed if you're changing accounts

Then:

1. Drop the _MUSER_ folder into the base WordPress directory (e.g., `public_html`).
1. Visit this URL: `{site}/MUSER/?muffinKey={key}`
1. Options are shown. Choose them with query strings. `0` = false, `1` = true.
    - For example, to check if a user can be created with the details you've supplied, append the current URL with `&action=check`
1. Do a dry run. The result will be shown without updating the DB
1. Delete when you're done, remember to backup

## Todo

- Post options, instead of manual query string editing
- Remove the random key requirement, encourage random renaming instead (check dir `!MUSER`)
- Add colour
