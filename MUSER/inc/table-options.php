<?php
// Site info table

function getOptionsTable( $options = null )
{
    $options_arr = array(
		array(
			'param'   => 'muffinKey',
			'name'    => 'Required for entry',
			'default' => '-'
		),
		array(
			'param'   => 'hideContent',
			'name'    => 'Hide all content except the user action messages',
			'default' => '0'
		),
		array(
			'param'   => 'listNonAdmin',
			'name'    => 'List non-admin users in a separate user table (not recommended with WooCommerce)',
			'default' => '0'
		),
		array(
			'param'   => 'liveRun',
			'name'    => 'Perform a live run. Default (0) is a dry run',
			'default' => '0'
		),
		array(
			'param'   => 'action',
			'name'    => 'User action to perform. See below for options',
			'default' => 'none'
		),
    );

    ob_start();
    ?>

		<h2>Options</h2>

        <table>
			<thead>
				<tr>
					<td>Parameter</td>
					<td>Option</td>
					<td>Default</td>
					<td>Current</td>
				</tr>
			</thead>
            <tbody>
				<?php foreach($options_arr as $option)
				{ ?>
					<tr>
						<td><?php echo $option['param']; ?></td>
						<td><?php echo $option['name']; ?></td>
						<td><?php echo $option['default']; ?></td>
						<td><?php echo $options[$option['param']]; ?>
					</tr>
				<?php } ?>
            </tbody>
        </table>

    <?php
    $output = ob_get_clean();
    return $output;
}

function getActionsTable( $options = null )
{
    $actions_arr = array(
		array(
			'action'   => 'check',
			'name'    => 'Check the supplied user details, but don\'t create the user',
		),
		array(
			'action'   => 'create',
			'name'    => 'Attempt to create a new user',
		),
		array(
			'action'   => 'changePass',
			'name'    => 'Update the password for the supplied username (slug/user_login)',
		),
    );

    ob_start();
    ?>

		<h2>Actions</h2>

        <table>
			<thead>
				<tr>
					<td>Action</td>
					<td>Option</td>
					<td>Current</td>
				</tr>
			</thead>
            <tbody>
				<?php foreach($actions_arr as $action)
				{
					?>
					<tr>
						<td><?php echo $action['action']; ?></td>
						<td><?php echo $action['name']; ?></td>
						<td><?php echo ( $action['action'] == $options['action'] ) ? 'true' : '' ; ?>
					</tr>
				<?php } ?>
            </tbody>
        </table>

    <?php
    $output = ob_get_clean();
    return $output;
}
