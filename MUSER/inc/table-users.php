<?php
// User info tables

function getUserInfoTables( $show_not_admin = 0 )
{
    $users_admin = get_users( array(
		'blog_id'  => $GLOBALS['blog_id'],
		'role__in' => 'administrator',
		'orderby'  => 'ID',
		'fields'   => 'all_with_meta'
	));

	$users_a = getUserInfoTable( $users_admin, 'Admin Users' );

	// Avoid a potentially costly call to fetch all users
	if ( $show_not_admin )
	{
		$users_not_admin = get_users( array(
			'blog_id'      => $GLOBALS['blog_id'],
			'role__not_in' => 'administrator',
			'orderby'      => 'ID'
		));

		if ( !empty( $users_not_admin ) )
		{
			$users_b = getUserInfoTable( $users_not_admin, 'Other Users' );
			return $users_a . $users_b;
		}
	}

	return $users_a;
}


function getUserInfoTable( $users, $title )
{
    ob_start();

    // User data to loop over
    $userinfo_arr = array(
        'ID',
        'user_login',
        'display_name',
        'user_nicename',
        'user_email',
        'user_registered',
    );
    ?>

    <h2><?php echo $title . ' (' . count( $users ) . ')'; ?></h2>

    <table>
        <thead>
            <tr>
                <?php
				echo '<td>Index</td>';

                foreach( $userinfo_arr as $user_info )
                {
                    echo '<td>' . $user_info . '</td>';
                }

                echo '<td>Role</td>';
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach( $users as $index => $user )
            {
                echo '<tr>';

					echo '<td>' . $index . '</td>';

                    foreach($userinfo_arr as $user_info)
                    {
                        echo '<td>' . $user->$user_info . '</td>';
                    }

                    echo '<td>' . implode(', ', $user->roles) . '</td>';

                echo '</tr>';
            } ?>
        </tbody>
    </table>

    <?php
    $output = ob_get_clean();
    return $output;
}
