<?php
// Site info table

function getSiteInfoTable()
{
    $bloginfo_arr = array(
        'name',
        'version',
        'admin_email'
    );

    ob_start();
    ?>

        <h2>Site Info</h2>

        <table>
			<thead>
				<tr>
					<td>Key</td>
					<td>Value</td>
				</tr>
			</thead>
            <tbody>
            <?php foreach( $bloginfo_arr as $info ) { ?>
                <tr>
                    <td><?php echo $info; ?></td>
                    <td><?php echo get_bloginfo( $info ); ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>

    <?php
    $output = ob_get_clean();
    return $output;
}
