<?php
/**
 * MUSER
 *
 * @author  Code Muffin
 * @version 1.0.0
 * @link    https://codemuffin.com
 */

// The key is required for access. It provides a small amount of protection
// against accidental deploys
require_once( 'key.php' );

if (!isset($_GET) || !isset($_GET['muffinKey']) || $_GET['muffinKey'] !== $muffin_muser_key)
{
	// header("HTTP/1.0 404 Not Found");
	die();
}

// Skip loading the theme files
define( 'WP_USE_THEMES', false );

// Load WP Core
require_once( dirname( __DIR__ ) . '/wp-blog-header.php' );

// Content functions
require_once( 'inc/table-options.php' );
require_once( 'inc/table-site.php' );
require_once( 'inc/table-users.php' );

// Defaults
$options = array(
	'action'       => "none",
	'hideContent'  => "0",
	'listNonAdmin' => "0",
	'liveRun'      => "0",
);

// Merge GET args with options
$options = isset( $_GET ) ? array_replace_recursive( $options, $_GET ) : $options;

?><!DOCTYPE html>
<html>
<head>
    <title>MUSER</title>
    <meta charset="utf-8" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
</head>
<body>
    <main>
        <?php
		// User actions
		if ( $options['action'] !== 'none' )
		{
			// $my_muffin_user
			require_once( 'user.php' );

			// User validation & update class
			require_once( 'classes/Muffin_Muser.php' );

			$muffin_user = new Muffin_Muser(
				$my_muffin_user['username'],
				$my_muffin_user['email'],
				$my_muffin_user['password'],
				$options['liveRun']
			);

			switch ( $options['action'] )
			{
				case 'check':
					$muffin_user->check();
					break;

				case 'create':
					$muffin_user->create();
					break;

				case 'changePass':
					$muffin_user->changePass();
					break;
			}
		}

		// Content
		if ( !$options['hideContent'] )
		{
			echo getOptionsTable( $options );
			echo getActionsTable( $options );
			echo getSiteInfoTable();
			echo getUserInfoTables( $options['listNonAdmin'] );
		} ?>
    </main>
</body>
