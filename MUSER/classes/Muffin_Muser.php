<?php

/**
 * Muffin_Muser class. Handles all user-related actions
 *
 * @version 1.0.0
 */
class Muffin_Muser
{
	public function __construct( $username = 'exampleuser', $email = 'exampleuser@example.com', $password = null, $live_run = false )
	{
		if ( !isset( $password ) )
		{
			echo 'Password is required';

			die();
		}

		// User args
		$this->username = $username;
		$this->email    = $email;
		$this->password = $password;
		$this->live_run = $live_run;

		// Status messages
		$this->messages = array();

		// Validation checks
		$this->username_exists = false;
		$this->email_exists = false;
		$this->can_create = true;

		// Validate the user: Check that both their supplied username and
		// password are not already in use
		$this->validate();
	}

	/**
	 * Check the supplied user details, but don't create the user
	 *
	 * @since 0.1
	 */
	public function check()
	{
		// Create the new user, if possible
		if ( $this->can_create )
		{
			$this->messages[]= $this->getMessage( 'check_success', null, true );
		}
		else
		{
			$this->messages[]= $this->getMessage( 'check_error', null, true );
		}

		$this->displayMessages( 'Check User' );
	}

	/**
	 * Attempt to create a new user
	 *
	 * @since 0.1
	 */
	public function create()
	{
		if ( $this->can_create )
		{
			if ( $this->live_run )
			{
				// Create the user
				$user_id = wp_create_user(
					$this->username,
					$this->password,
					$this->email
				);

				// Set the new user's role
				$wp_user = new WP_User( $user_id );
				$wp_user->set_role( 'administrator' );
			}

			$this->messages[]= $this->getMessage( 'create_success', null, true );
			$this->addDeleteMessage();
		}
		else
		{
			$this->messages[]= $this->getMessage( 'create_error', null, true );
		}

		$this->displayMessages( 'Create User' );
	}

	/**
	 * Change the user's password
	 *
	 * @since 0.1
	 */
	public function changePass()
	{
		if ( $this->username_exists )
		{
			// Get the user ID
			$wp_user = get_user_by( 'slug', $this->username );
			$wp_user_id = $wp_user->ID;

			if ( $this->live_run )
			{
				// Change this user's password
				wp_set_password( $this->password, $wp_user_id );
			}

			$this->messages[]= $this->getMessage( 'pass_success', null, true );
			$this->addDeleteMessage();
		}
		else
		{
			$this->messages[]= $this->getMessage( 'pass_error', null, true );
		}

		$this->displayMessages( 'Change Password' );
	}

	/**
	 * Validate user details
	 *
	 * Checks that their username and email address aren't already in use
	 *
	 * @since 0.1
	 *
	 * @return void
	 */
	private function validate()
	{
		$username = $this->username;
		$email    = $this->email;

		if ( !username_exists( $username ) )
		{
			$this->messages[]= $this->getMessage( 'username_available', $username );
		}
		else
		{
			$this->messages[]= $this->getMessage( 'username_exists', $username );
			$this->username_exists = true;
			$this->can_create = false;
		}

		if ( !email_exists( $email ) )
		{
			$this->messages[]= $this->getMessage( 'email_available', $email );
		}
		else
		{
			$this->messages[]= $this->getMessage( 'email_exists', $email );
			$this->email_exists = true;
			$this->can_create = false;
		}
	}

	/**
	 * Adds a "delete this script" notice to the messages array, if this is a
	 * live run. Does nothing for dry runs.
	 */
	private function addDeleteMessage()
	{
		if ( $this->live_run )
		{
			$this->messages[]= $this->getMessage( 'delete', null, true );
		}
	}

	/**
	 * Get a status message's HTML from its status key
	 *
	 * @since 0.1
	 *
	 * @param   string  $key          Message ID
	 * @param   string  $data         Text data. Used by some messages
	 * @param   bool    $breakBefore  True to insert a br before the message
	 *
	 * @return  string                HTML message
	 */
	private function getMessage( $key, $data, $breakBefore = false )
	{
		$html_messages = array(
			'username_available' => 'Username available: ' . $this->wrapText( 'success', $data ),
			'username_exists'    => 'Username exists: '    . $this->wrapText( 'error',   $data ),

			'email_available'    => 'Email available: &nbsp;&nbsp;&nbsp;' . $this->wrapText( 'success', $data ),
			'email_exists'       => 'Email exists:    &nbsp;&nbsp;&nbsp;' . $this->wrapText( 'error',   $data ),

			'check_success'      => $this->wrapText( 'success', 'This user account can be created' ),
			'check_error'        => $this->wrapText( 'error',   'This user account cannot be created, because either the username or email are already in use' ),

			'create_success'     => $this->wrapText( 'success', 'SUCCESS. The new user account has been created' ),
			'create_error'       => $this->wrapText( 'error',   'FAILURE. This user account could not be created, because either the username or email are already in use' ),

			'pass_success'       => $this->wrapText( 'success', 'SUCCESS. The user\'s password has been changed' ),
			'pass_error'         => $this->wrapText( 'error',   'FAILURE. The user\'s password could not be changed, because the user does not exist' ),

			'delete'             => $this->wrapText( 'warning', '<strong>Please delete this script now</strong>' ),
		);

		$output = ( $breakBefore ? '<br>' : '' ) . ( ( array_key_exists( $key, $html_messages ) ) ? $html_messages[$key] : '' );

		return $output;
	}

	/**
	 * Wrap part of the status message
	 *
	 * @since 0.1
	 *
	 * @param   string  $type  Class type. Corresponds with a CSS class.
	 * @param   string  $data  Text data. Wrapped in a `<code>` tag
	 *
	 * @return  string         HTML string
	 */
	private function wrapText( $type, $data )
	{
		return '<span class="mfn-cnu-' . $type . '"><code>' . $data . '</code></span>';
	}

	/**
	 * Echo the status messaages
	 *
	 * @since 0.1
	 */
	private function displayMessages( $heading )
	{
		$run = ' ' . ( $this->live_run  ? '(LIVE RUN)' : '(DRY RUN)' );
		?>

		<div class="mfn-cnu">
			<h2 class="mfn-cnu-heading"><?php echo $heading . $run; ?></h2>
			<div class="mfn-cns-notice"><?php echo implode( '<br>' . "\n", $this->messages ); ?></div>
		</div>
	<?php }
}
